﻿using GZipCompressor;
using System;
using System.Text;

namespace GZipTest
{

    public static class ProgressBar
    {
        public static void DrawTextProgressBar(object sender, IProgress barInfo)
        {
            Console.CursorVisible = false;
            ConsoleColor curBackground = Console.BackgroundColor;
            ConsoleColor curForeground = Console.ForegroundColor;

            if (barInfo.Total == 0) barInfo.Total = 1;
            double percent = ((double)barInfo.Current / barInfo.Total);
            double currPosition = percent * 40;

            StringBuilder sb = new StringBuilder();
            sb.Append($"Выполнено {percent:P0}\t[ ");
            sb.Append(new string('█', (int)currPosition));
            sb.Append(new string('░', 40 - (int)currPosition));
            sb.Append(" ]");

            Console.CursorLeft = 0;
            Console.Write(sb);
            Console.CursorVisible = true;            
        }
    }

}
