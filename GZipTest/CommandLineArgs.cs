﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace GZipTest
{
    public enum CompressMode
    {
        Undefined,
        Compress,
        Decompress
    }

    public class CommandLineArgs
    {
        public CompressMode CompressMode { get; set; }
        public string Source { get; set; }
        public string Target { get; set; }
        public bool IsValid { get; set; }
        public bool ShowProgressBar { get; set; } = true;
        public bool Wait { get; set; } = false;

        private readonly string lineArgs;

        public CommandLineArgs(string[] args = null, bool parse = true)
        {
            if (args == null)
            {
                string[] comArgs = Environment.GetCommandLineArgs();
                args = new string[comArgs.Length - 1];
                Array.Copy(comArgs, 1, args, 0, comArgs.Length - 1);
            }

            if (args.Length < 2)
            {
                this.IsValid = false;
                return;
            }

            lineArgs = string.Join(" ", args).Trim();
            if (parse)
            {
                this.IsValid = Parse();
            }
        }

        /// <summary>
        /// Разобрать аргументы командной строки
        /// </summary>
        /// <returns></returns>
        public bool Parse()
        {
            string pattern = @"^(?<mode>(?:de)?compress)\s(?<source>"".*?""|[^ ""]+)\s?(?<target>"".*?""|(?!-w|-wp)[^ ""]+)?(?<otherParams>.*)";
            Regex regex = new Regex(pattern, RegexOptions.IgnoreCase);
            Match match = regex.Match(lineArgs);

            if (!match.Success)
            {
                return false;
            }
            else
            {
                string mode = match.Groups["mode"].Value;
                string source = match.Groups["source"].Value.Trim('\"');
                string target = match.Groups["target"].Value.Trim('\"');
                string[] otherParams = match.Groups["otherParams"].Value.Split(' ');

                if (!Enum.TryParse(mode, true, out CompressMode compressMode))
                {
                    return false;
                }
                else
                {
                    this.CompressMode = compressMode;
                }

                if (string.IsNullOrWhiteSpace(target))
                {
                    target = FillNameTarget(source);
                }

                this.ShowProgressBar = otherParams.FirstOrDefault(p => p == "-wp") == null ? true : false;
                this.Wait = otherParams.FirstOrDefault(p => p == "-w") == null ? false : true;

                this.Source = source;
                this.Target = target;

                return true;
            }       
        }

        /// <summary>
        /// Заполнить имя файла назначения при его отсутствии
        /// </summary>
        /// <param name="source"></param>
        /// <returns></returns>
        private string FillNameTarget(string source)
        {
            if (this.CompressMode == CompressMode.Compress)
            {
                return source + ".gz";
            }
            else
            {
                bool isGzExt = source.EndsWith(".gz", StringComparison.OrdinalIgnoreCase);
                if (isGzExt)
                {
                    return source.Substring(0, source.IndexOf(".gz"));
                }
                else
                {
                    bool hasExt = source.LastIndexOf('.') != -1;
                    return hasExt
                        ? source.Insert(source.LastIndexOf('.'), "_dec")
                        : source + "_dec";                   
                }
            }
 
        }

    }
}
