﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GZipTest
{
    public static class CheckFiles
    {
        /// <summary>
        /// Проверка файлов источника и назначения
        /// </summary>
        /// <param name="args"></param>
        /// <returns></returns>
        public static bool IsValid(CommandLineArgs args)
        {
            FileInfo sourceInfo = new FileInfo(args.Source);
            FileInfo targetInfo = new FileInfo(args.Target);

            if (!sourceInfo.Exists)
            {
                Console.WriteLine($"Файла с именем {sourceInfo.Name} нет по указанному пути {sourceInfo.DirectoryName}");
                return false;
            }
            else
            {
                if (sourceInfo.Length == 0)
                {
                    using (StreamWriter sw = sourceInfo.AppendText())
                    {
                        sw.Write((char)0x00);
                    }
                }
            }

            if (targetInfo.Exists)
            {
                Console.WriteLine($"Файл назначение {targetInfo.Name} уже существует.");
                Console.Write("Выполнить перезапись? [Y/N]: ");
                var answer = Console.ReadLine();
                if (answer.Equals("y", StringComparison.CurrentCultureIgnoreCase))
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }

            return true;
        }
    }
}
