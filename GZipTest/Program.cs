﻿using GZipCompressor;
using System;
using System.Diagnostics;
using System.Text;
using System.Threading;

namespace GZipTest
{

    class Program
    {
        static int Main(string[] args)
        {
            Console.OutputEncoding = Encoding.Unicode;

            CommandLineArgs commandLine = new CommandLineArgs(args, true);
            if (commandLine.IsValid && CheckFiles.IsValid(commandLine))
            {
                return StartOperation(commandLine);
            }
            else
            {
                if (!commandLine.IsValid) ShowHelpMsg();
                return 1;
            }
        }

        /// <summary>
        /// Запустить операцию сжатия или разархивирования
        /// </summary>
        /// <param name="commandLine"></param>
        /// <returns></returns>
        private static int StartOperation(CommandLineArgs commandLine)
        {
            int result = 0;

            Func<int> gzipAction = null;
            EventHandler<IProgress> progress = null;
            if (commandLine.ShowProgressBar)
            {
                progress = ProgressBar.DrawTextProgressBar;
            }

            ICompressor gZip = new GZip(commandLine.Source, commandLine.Target, progress);
            string opName = "";

            switch (commandLine.CompressMode)
            {
                case CompressMode.Compress: gzipAction = gZip.Compress; opName = "сжатие"; break;
                case CompressMode.Decompress: gzipAction = gZip.Decompress; opName = "распаковка"; break;
            }

            Stopwatch sw = new Stopwatch();
            sw.Start();
            try
            {
                Console.WriteLine();
                Console.WriteLine($"Выполняется {opName} {commandLine.Source} в файл {commandLine.Target}");

                Logger.Info($"GZip {commandLine.CompressMode} start {commandLine.Source}", "Main");
                result = gzipAction.Invoke();
                Logger.Info($"GZip {commandLine.CompressMode} finish {commandLine.Target}", "Main");
            }
            catch (ApplicationException appEx)
            {
                Console.WriteLine(appEx.Message);
                Logger.Error(appEx);
                result = 1;
            }
            catch (SystemException sysEx)
            {
                Console.WriteLine(sysEx.Message);
                Logger.Error(sysEx);
                result = 1;
            }
            sw.Stop();

            if (result == 0)
            {
                Thread.Sleep(100);

                Console.WriteLine();
                Console.WriteLine();
                Console.WriteLine($"Время выполнения {sw.ElapsedMilliseconds} мс - {sw.Elapsed}");
            }

            if (commandLine.Wait)
            {
                Console.WriteLine("Для продолжения нажмите любую клавишу . . . ");
                Console.ReadKey();
            }
            return result;
        }

        /// <summary>
        /// Отобразить окно помощи
        /// </summary>
        private static void ShowHelpMsg()
        {
            string m = "Выполняет архивирование и разархивирование файла в формате GZip\n" +
                Environment.NewLine +
                "GZipTest.exe [compress]/[decompress] [имя исходного файла] [имя результирующего файла]\n" +
                Environment.NewLine +
                "\t[имя исходного файла] - архивируемый файл\n" +
                "\t[имя результирующего файла] - имя архивного файла (опционально)\n" +
                "\tcompress - сжатие файла\n" +
                "\tdecompress - распаковка файла\n";

            Console.WriteLine(m);
        }

    }
}
