﻿using GZipTest;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GZipXUnitTest
{
    public abstract class CommonFields
    {
        public CompressMode CompressMode;
        public string Source;
        public string Target;
    }
}
