﻿using GZipCompressor;
using GZipTest;
using Xunit;


namespace GZipXUnitTest
{

    public class GZipParamTests : CommonFields
    {

        private bool IsValidParam(string[] args)
        {
            Utils.SetDefaultValue(this);
            CommandLineArgs commandLine = new CommandLineArgs(args, true);
            CompressMode = commandLine.CompressMode;

            return commandLine.IsValid;
        }

        //правильный параметр     
        [Fact]
        public void CheckParamsTest1()
        {
            string[] paramTest1 = { ("decompress"), ("source"), ("target.gz") };              
            Assert.True(IsValidParam(paramTest1));
            Assert.Equal("Decompress", CompressMode.ToString());
        }

        //правильный параметр     
        [Fact]
        public void CheckParamsTest2()
        {
            string[] paramTest2 = { ("compress"), ("source"), ("target.gz") }; 
            Assert.True(IsValidParam(paramTest2));
            Assert.Equal("Compress", CompressMode.ToString());
        }

        //Опечатка
        [Fact]
        public void CheckParamsTest3()
        {
            string[] paramTest3 = { ("hicompress"), ("source"), ("target.gz") };
            Assert.False(IsValidParam(paramTest3));
            Assert.NotEqual("Compress", CompressMode.ToString());
        }

        //отсутствие параметра
        [Fact]
        public void CheckParamsTest4()
        {
            string[] paramTest4 = { (""), ("source"), ("target.gz") };
            Assert.False(IsValidParam(paramTest4));
        }

        //неправильное значение параметра 
        [Fact]
        public void CheckParamsTest5()
        {
            string[] paramTest5 = { ("сжатие"), ("source"), ("target.gz") }; 
            Assert.False(IsValidParam(paramTest5));
        }

        //Дополнительные параметры
        [Fact]
        public void CheckParamsTest6()
        {
            string[] paramTest6 = { ("compress"), ("source"), ("target.gz"), ("-w") };       
            Assert.True(IsValidParam(paramTest6));
        }
    }

    public class CheckPathTests : CommonFields
    {
        private bool IsValidPath(string[] args)
        {
            Utils.SetDefaultValue(this);
            CommandLineArgs commandLine = new CommandLineArgs(args, true);
            this.CompressMode = commandLine.CompressMode;
            this.Source = commandLine.Source;
            this.Target = commandLine.Target;

            return commandLine.IsValid;
        }

        //Проверка источника и назначения с короткими путями
        [Fact]
        public void CheckPathTest1()
        {
            string[] pathTest1 = { ("compress"), ("ShortName\\source"), ("ShortName\\target.gz") };

            Assert.True(IsValidPath(pathTest1));
            Assert.Equal("ShortName\\source", Source);
            Assert.Equal("ShortName\\target.gz", Target);
        }

        //Проверка пути источника с длинным именем и пробелами в названии 
        //и пути назначения с коротким именем
        [Fact]
        public void CheckPathTest2()
        {
            string folderPath = System.AppDomain.CurrentDomain.BaseDirectory;
            string[] pathTest2 = { ("compress"), ("\"" + folderPath + "\\Dir With Spaces\\source. hi.pdf\""), ("target.gz") };

            Assert.True(IsValidPath(pathTest2));
            Assert.Equal(folderPath + "\\Dir With Spaces\\source. hi.pdf", Source);
            Assert.Equal("target.gz", Target);
        }

        //Проверка путей источника и назначение с длинными именами 
        //и пробелами в названии
        [Fact]
        public void CheckPathTest3()
        {
            string folderPath = System.AppDomain.CurrentDomain.BaseDirectory;
            string[] pathTest3 = 
                { ("compress"), ("\"" + folderPath + "\\Dir With Spaces\\source\""),
                ("\"" + folderPath + "\\Dir With Spaces\\target.gz\"") };

            Assert.True(IsValidPath(pathTest3));
            Assert.Equal(folderPath + "\\Dir With Spaces\\source", Source);
            Assert.Equal(folderPath + "\\Dir With Spaces\\target.gz", Target);
        }

        
        //Проверка коротких путей с пробелами в названиях папки
        [Fact]
        public void CheckPathTest4()
        {
            string[] pathTest4 = { ("compress"), ("\"Dir With Spaces\\source\""), ("\"Dir With Spaces\\target.gz\"") };

            Assert.True(IsValidPath(pathTest4));
            Assert.Equal("Dir With Spaces\\source", Source);
            Assert.Equal("Dir With Spaces\\target.gz", Target);
        }

         
        //Проверка пути с пробелами в названиях папки
        //и отсутствием пути назначения
        [Fact]
        public void CheckPathTest5()
        {
            string[] pathTest5 = { ("compress"), ("\"Dir With Spaces\\source\"") };

            Assert.True(IsValidPath(pathTest5));
            Assert.Equal("Dir With Spaces\\source", Source);
            Assert.Equal("Dir With Spaces\\source.gz", Target);
        }

        //Проверка отсутствия путей источника и назначения
        [Fact]
        public void CheckPathTest6()
        {
            string[] pathTest6 = { ("compress") };

            Assert.False(IsValidPath(pathTest6));
        }

        //Проверка пути источника и отсутствие пути назначения
        //источник без расширения
        [Fact]
        public void CheckPathTest7()
        {
            string[] pathTest7 = { ("decompress"), ("ShortName\\source") };

            Assert.True(IsValidPath(pathTest7));
            Assert.Equal("ShortName\\source", Source);
            Assert.Equal("ShortName\\source_dec", Target);
        }

        //Проверка пути источника и отсутствие пути назначения
        //источник с расширением gz
        [Fact]
        public void CheckPathTest8()
        {
            string[] pathTest8 = { ("decompress"), ("ShortName\\source.gz") };

            Assert.True(IsValidPath(pathTest8));
            Assert.Equal("ShortName\\source.gz", Source);
            Assert.Equal("ShortName\\source", Target);
        }

        //Проверка пути источника и отсутствие пути назначения
        //источник с расширением someArchive
        [Fact]
        public void CheckPathTest9()
        {
            string[] pathTest9 = { ("decompress"), ("ShortName\\source.someArchive") };

            Assert.True(IsValidPath(pathTest9));
            Assert.Equal("ShortName\\source.someArchive", Source);
            Assert.Equal("ShortName\\source_dec.someArchive", Target);
        }
    }

    public class CompressDecompressTests : CommonFields
    {
        private bool IsValidFile(string filePartName, int size)
        {
            string originTestFile = filePartName + ".bin";
            string compressTestFile = originTestFile + ".gz";
            string decompressTestFile = filePartName + "_dec.bin";

            Utils.CreateTestFile(originTestFile, true, size);
            
            GZip gZip = new GZip(originTestFile, compressTestFile);
            gZip.Compress();
            GZip gZip2 = new GZip(compressTestFile, decompressTestFile);
            gZip2.Decompress();

            bool result = Utils.FileCompare(originTestFile, decompressTestFile);
            Utils.DeleteTestFile(originTestFile);
            Utils.DeleteTestFile(compressTestFile);
            Utils.DeleteTestFile(decompressTestFile);

            return result;
        }

        [Fact]
        public void CompressDecompressTest1k()
        {
            Assert.True(IsValidFile("Test1k", 1024));
        }

        [Fact]
        public void CompressDecompressTest1M()
        {
            Assert.True(IsValidFile("Test1M", 1048576));
        }

        [Fact]
        public void CompressDecompressTest5M()
        {
            Assert.True(IsValidFile("Test5M", 5242880));
        }

        [Fact]
        public void CompressDecompressTest10M()
        {
            Assert.True(IsValidFile("Test10M", 10485760));
        }

        [Fact]
        public void CompressDecompressTest100M()
        {
            Assert.True(IsValidFile("Test100M", 104857600));
        }

        [Fact]
        public void CompressDecompressTest1G()
        {
            Assert.True(IsValidFile("Test1G", 1073741824));
        }

    }

}
