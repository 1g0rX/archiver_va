﻿using GZipTest;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;

namespace GZipXUnitTest
{
    static class Utils
    {
        static readonly int partSize = 4194304;

        public static void CreateTestFile(string fileName, bool withData = false, int dataSize = 0)
        {
            if (File.Exists(fileName)) return;

            using (FileStream stream = new FileStream(fileName, FileMode.OpenOrCreate))
            {
                if (withData && dataSize != 0)
                {
                    Random random = new Random();
                    do
                    {
                        int size = Math.Min(partSize, dataSize - (int)stream.Position);
                        byte[] buffer = new byte[size];
                        random.NextBytes(buffer);
                        stream.Write(buffer, 0, size);

                    } while ((dataSize - stream.Position) != 0); 
                }
            }
        }

        public static void DeleteTestFile(string fileName)
        {
            if (File.Exists(fileName))
            {
                File.Delete(fileName);
            }
        }

        public static void CreateTestDirectory(string directory)
        {
            if (!Directory.Exists(directory))
            {
                Directory.CreateDirectory(directory);
            }
        }

        public static void DeleteTestDirectory(string directory)
        {
            if (Directory.Exists(directory))
            {
                Directory.Delete(directory);
            }
        }

        /// <summary>
        /// Установить значения по-умолчанию
        /// </summary>
        /// <param name="properties"></param>
        public static void SetDefaultValue(CommonFields properties)
        {
            properties.Target = null;
            properties.Source = null;
            properties.CompressMode = CompressMode.Undefined;
        }

        static string CalculateMD5(string filename)
        {
            using (var md5 = MD5.Create())
            {
                using (var stream = File.OpenRead(filename))
                {
                    var hash = md5.ComputeHash(stream);
                    return BitConverter.ToString(hash).Replace("-", "").ToLowerInvariant();
                }
            }
        }

        /// <summary>
        /// Сравнить два файла по содержимому
        /// </summary>
        /// <param name="file1"></param>
        /// <param name="file2"></param>
        /// <returns></returns>
        public static bool FileCompare(string file1, string file2)
        {
            string file1hash = CalculateMD5(file1);
            string file2hash = CalculateMD5(file2);

            return file1hash.Equals(file2hash, StringComparison.OrdinalIgnoreCase);
        }
    }
}
