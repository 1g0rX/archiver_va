﻿using System; 
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GZipCompressor
{
    //Класс чтения сжатых данных
    class GZipReader : IReader
    {
        FileStream fileStream;
        IDataProcessor dataProcessor;

        int idx = 0;

        public GZipReader(FileStream stream, IDataProcessor dataContainer)
        {
            this.fileStream = stream;
            this.dataProcessor = dataContainer;
        }

        public void Start(Action<Exception> handler)
        {
            while (fileStream.Length > fileStream.Position)
            {
                if (dataProcessor.IsError) break;
                
                int partionSize = CalcPartionSize(this.fileStream);
                byte[] buffer = new byte[partionSize];
                try
                {
                    if (partionSize == 0) throw new GZipZeroPartException();

                    fileStream.Read(buffer, 0, partionSize);
                }
                catch (Exception ex)
                {
                    dataProcessor.SetError();
                    handler(ex);
                    break;
                }

                PartElement partElement = new PartElement { Idx = this.idx, Buffer = buffer };
                dataProcessor.AddReaderBlock(partElement);
                idx++;      
            }
            dataProcessor.EndReading();
        }

        /// <summary>
        /// Вычислить размер раздела
        /// </summary>
        /// <param name="stream"></param>
        /// <returns></returns>
        private int CalcPartionSize(Stream stream)
        {
            int partionSize;

            stream.Seek(4, SeekOrigin.Current);
            byte[] b = new byte[4];
            stream.Read(b, 0, 4);
            partionSize = BitConverter.ToInt32(b, 0);
            stream.Seek(-8, SeekOrigin.Current);

            return partionSize;
        }
    }
}
