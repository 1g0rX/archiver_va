﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace GZipCompressor
{
    class DataProcessor : IDataProcessor
    {
        readonly int maxElements = 100;

        public bool IsError { get; private set; } = false;

        Queue<PartElement> inputElements = new Queue<PartElement>();        
        HashSet<PartElement> outputElements = new HashSet<PartElement>();

        public ManualResetEventSlim addInElementsEvent = new ManualResetEventSlim(true);
        public ManualResetEventSlim addOutElementsEvent = new ManualResetEventSlim(true);
        public ManualResetEventSlim endReadingEvent = new ManualResetEventSlim(false);
        public ManualResetEventSlim readyToWriteEvent = new ManualResetEventSlim(true);

        event EventHandler<IProgress> Notify;
        readonly int notifyFrequency;

        readonly object inLockObj = new object();
        readonly object outLockObj = new object();
        int writeIdx = 0;
        int readIdx = 0;
        long readData = 0;
        readonly long length = 0;

        public DataProcessor(EventHandler<IProgress> notify = null, int notifyFrequency=1, long length = 0)
        {
            this.Notify += notify;
            this.notifyFrequency = notifyFrequency;
            this.length = length;
        }

        /// <summary>
        /// Добавить обработанный блок
        /// </summary>
        /// <param name="element"></param>
        public void AddProcessedBlock(PartElement element)
        {
            addOutElementsEvent.Wait();

            lock (outLockObj)
            {
                outputElements.Add(element);

                if (!readyToWriteEvent.IsSet && writeIdx == element.Idx)
                {
                    readyToWriteEvent.Set();
                }

                if (outputElements.Count >= maxElements)
                {
                    addOutElementsEvent.Reset();
                }
            }
        }

        /// <summary>
        /// Добавить блок данных исходного файла
        /// </summary>
        /// <param name="element"></param>
        public void AddReaderBlock(PartElement element)
        {
            addInElementsEvent.Wait();

            lock (inLockObj)
            {
                inputElements.Enqueue(element);
                readIdx++;
                readData += element.Buffer.Length;

                if (inputElements.Count >= maxElements)
                {
                    addInElementsEvent.Reset();
                }
            }
        }

        /// <summary>
        /// Получить блок данных для обработки
        /// </summary>
        /// <returns></returns>
        public PartElement GetProcessingBlock()
        {
            addInElementsEvent.Set();

            lock (inLockObj)
            {
                if (endReadingEvent.IsSet && inputElements.Count == 0)
                {
                    return new PartElement { Idx = -1, Buffer = null };
                }

                return inputElements.Count == 0 ? new PartElement { Idx = -2, Buffer = null } : inputElements.Dequeue();
            }
        }

        /// <summary>
        /// Получить блок данных для записи
        /// </summary>
        /// <returns></returns>
        public PartElement GetWriterBlock()
        {
            PartElement element;

            if (writeIdx % notifyFrequency == 0)
            {
                Notify?.Invoke(this, new ProgressInfo { Current = readData, Total = length });
            }

            do
            {
                if (IsError)
                {
                    element = new PartElement { Idx = -1, Buffer = null };
                    break;
                }
                readyToWriteEvent.Wait();

                lock (outLockObj)
                {
                    if (endReadingEvent.IsSet && writeIdx == readIdx)
                        return new PartElement { Idx = -1, Buffer = null };

                    element = outputElements.FirstOrDefault(p => p.Idx == this.writeIdx);
                    if (element == null)
                    {
                        readyToWriteEvent.Reset();
                        if (outputElements.Count >= maxElements)
                            addOutElementsEvent.Set();
                    }
                    else
                    {
                        if (outputElements.Count < maxElements)
                            addOutElementsEvent.Set();

                        outputElements.Remove(element);
                        if (outputElements.Count == 0 && !endReadingEvent.IsSet)
                            readyToWriteEvent.Reset();
                        writeIdx++;
                    }
                }
            }
            while (element == null);

            return element;
        }

        /// <summary>
        /// Завершение чтения файла
        /// </summary>
        public void EndReading()
        {
            endReadingEvent.Set();
        }

        public void SetError()
        {
            IsError = true;
        }

    }
}
