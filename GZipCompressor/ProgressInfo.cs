﻿namespace GZipCompressor
{
    public class ProgressInfo : IProgress
    {
        public long Current { get; set; }
        public long Total { get; set; }
    }
}