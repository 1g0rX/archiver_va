﻿using System;
using System.Collections.Generic;
using System.IO;
using System.IO.Compression;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace GZipCompressor
{
    public class GZip : ICompressor
    {
        public string SourcePath { get; set; }
        public string TargetPath { get; set; }
        public static string Extension => "gz";

        private event EventHandler<IProgress> notify;
        private EventHandler<IProgress> progress;

        private GZipCore gZipCore;


        public GZip(string sourcePath, string targetPath = null, EventHandler<IProgress> progress = null)
        {
            this.SourcePath = sourcePath;
            this.TargetPath = targetPath;
            this.notify += progress;
            this.progress = progress;
            this.gZipCore = new GZipCore();
        }

        /// <summary>
        ///  Выполнить сжатие файла
        /// </summary>
        /// <returns></returns>
        public int Compress()
        {
            return DoOperation(CompressionMode.Compress);
        }

        /// <summary>
        /// Выполнить распаковку файла
        /// </summary>
        /// <returns></returns>
        public int Decompress()
        {
            return DoOperation(CompressionMode.Decompress);
        }

        /// <summary>
        /// Выполнить операция архивирования или разархивирования файла
        /// </summary>
        /// <param name="mode"></param>
        private int DoOperation(CompressionMode mode)
        {
            int result = 0;
            gZipCore.CompressionMode = mode;

            //поток для чтения файла источника
            using (FileStream sourceStream = new FileStream(SourcePath, FileMode.OpenOrCreate))
            {
                if (gZipCore.CompressionMode == CompressionMode.Decompress && !IsGZip(sourceStream))
                {
                    throw new GZipFormatException();
                }

                // поток для записи файла назначение
                using (FileStream targetStream = File.Create(TargetPath))
                {
                    gZipCore.CompressionMode = mode;
                    result = gZipCore.StartThreads(sourceStream, targetStream, this.progress);
                    notify?.Invoke(this, new ProgressInfo { Current = sourceStream.Position, Total = sourceStream.Length });
                }
            }
            return result;
        }

        /// <summary>
        /// Проверяет заголовок файла на соответствие формату GZip
        /// </summary>
        /// <param name="array">Заголовок</param>
        /// <returns></returns>
        private bool IsGZip(Stream stream)
        {
            byte[] header = new byte[2];
            stream.Read(header, 0, 2);
            stream.Position = 0;

            return header.Length >= 2 && header[0] == 0x1f && header[1] == 0x8b;
        }
    }
}
