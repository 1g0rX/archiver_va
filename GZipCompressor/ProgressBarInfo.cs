﻿namespace GZipCompressor
{
    public class ProgressBarInfo : IProgress
    {
        public long Current { get; set; }
        public long Total { get; set; }
    }
}
