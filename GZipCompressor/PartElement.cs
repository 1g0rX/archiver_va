﻿namespace GZipCompressor
{
    class PartElement
    {
        public int Idx { get; set; }
        public byte[] Buffer { get; set; }

    }
}
