﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GZipCompressor
{
    //класс записи сжатых данных
    class GZipWriter : IWriter
    {
        FileStream fileStream;
        IDataProcessor dataProcessor;

        public GZipWriter(FileStream stream, IDataProcessor dataContainer)
        {
            this.fileStream = stream;
            this.dataProcessor = dataContainer;
        }

        public void Start(Action<Exception> handler)
        {
            PartElement partElement;
            do
            {
                if (dataProcessor.IsError) break;

                partElement = dataProcessor.GetWriterBlock();
                if (partElement != null && partElement.Idx != -1)
                {
                    if (partElement.Buffer.Length != 0)
                    {
                        byte[] partSize = BitConverter.GetBytes((int)partElement.Buffer.Length);
                        Array.Copy(partSize, 0, partElement.Buffer, 4, partSize.Length);
                    }
                    try
                    {
                        fileStream.Write(partElement.Buffer, 0, partElement.Buffer.Length);
                    }
                    catch (Exception ex)
                    {
                        dataProcessor.SetError();
                        handler(ex);
                        break;
                    }
                }
            }    
            while (partElement.Idx != -1);

        }
    }
}
