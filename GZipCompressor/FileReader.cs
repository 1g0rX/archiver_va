﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace GZipCompressor
{
    //Класс чтения несжатых данных
    class FileReader : IReader
    {
        FileStream fileStream;
        IDataProcessor dataProcessor;

        readonly int readBlockSize = 1048576; // 1 Мб
        int idx = 0;
        public FileReader(FileStream stream, IDataProcessor dataContainer)
        {
            this.fileStream = stream;
            this.dataProcessor = dataContainer;
        }
        public void Start(Action<Exception> handler)
        {
            while (fileStream.Length > fileStream.Position)
            {
                if (dataProcessor.IsError) break;

                int partionSize = CalcPartionSize(this.fileStream);
                byte[] buffer = new byte[partionSize];
                try
                {
                    fileStream.Read(buffer, 0, partionSize);
                }
                catch (Exception ex)
                {
                    dataProcessor.SetError();
                    handler(ex);
                    break;
                }

                PartElement partElement = new PartElement { Idx = this.idx, Buffer = buffer };
                dataProcessor.AddReaderBlock(partElement);
                idx++;
            }
            dataProcessor.EndReading();
        }

        /// <summary>
        /// Вычислить размер раздела
        /// </summary>
        /// <param name="stream"></param>
        /// <returns></returns>
        private int CalcPartionSize(Stream stream)
        {
            int partionSize;

            partionSize = (int)Math.Min(readBlockSize, stream.Length - stream.Position);
            long leftPartSize = stream.Length - (stream.Position + partionSize);
            if (leftPartSize < readBlockSize)
            {
                partionSize = (int)(stream.Length - stream.Position);
            }

            return partionSize;
        }
    }
}
