﻿using System;

namespace GZipCompressor
{
    [Serializable]
    class GZipFormatException : ApplicationException
    {
        public GZipFormatException() : base("Файл не соответствует формату GZip")
        {

        }
    }

    [Serializable]
    class GZipZeroPartException : ApplicationException
    {
        public GZipZeroPartException() : base("Нулевой размер участка архива")
        {

        }
    }

}
