﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GZipCompressor
{
    //класс записи распакованных данных
    class FileWriter : IWriter
    {
        FileStream fileStream;
        IDataProcessor dataProcessor;

        public FileWriter(FileStream stream, IDataProcessor dataContainer)
        {
            this.fileStream = stream;
            this.dataProcessor = dataContainer;
        }

        public void Start(Action<Exception> handler)
        {
            PartElement partElement;
            do
            {
                if (dataProcessor.IsError) break;

                partElement = dataProcessor.GetWriterBlock();
                if (partElement != null && partElement.Idx != -1)
                {
                    try
                    {
                        fileStream.Write(partElement.Buffer, 0, partElement.Buffer.Length);
                    }
                    catch (Exception ex)
                    {
                        dataProcessor.SetError();
                        handler(ex);
                        break;
                    }
                }
            }
            while (partElement.Idx != -1);
        }
    }
}
