﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GZipCompressor
{
    public interface ICompressor
    {
        string SourcePath { get; set; }
        string TargetPath { get; set; }

        void Compress();
        void Decompress();
    }
}
