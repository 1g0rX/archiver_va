﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GZipCompressor
{
    public interface IProgress
    {
        long Current { get; set; }
        long Total { get; set; }
    }
}
