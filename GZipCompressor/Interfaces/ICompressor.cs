﻿namespace GZipCompressor
{
    public interface ICompressor
    {
        string SourcePath { get; set; }
        string TargetPath { get; set; }

        int Compress();
        int Decompress();
    }
}