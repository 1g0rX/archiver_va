﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GZipCompressor
{
    interface IDataProcessor
    {

        void AddReaderBlock(PartElement element);
        void AddProcessedBlock(PartElement element);
        PartElement GetProcessingBlock();
        PartElement GetWriterBlock();

        void EndReading();
    }
}
