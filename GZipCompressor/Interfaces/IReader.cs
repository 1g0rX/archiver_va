﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GZipCompressor
{
    interface IReader
    {
        void Start(Action<Exception> handler);
    }
}
