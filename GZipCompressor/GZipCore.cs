﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.IO.Compression;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace GZipCompressor
{
    class GZipCore
    {
        CountdownEvent countdown;
        IDataProcessor dateProcessor;
        int errorsFlag = 0;

        public CompressionMode CompressionMode { get; set; } = CompressionMode.Compress;
        public int ThreadsCount { get; private set; } = Environment.ProcessorCount;

        /// <summary>
        /// Создать и запустить потоки сжатия или разархивирования 
        /// </summary>
        /// <param name="sourceStream"></param>
        /// <param name="targetStream"></param>
        /// <param name="notify"></param>
        public int StartThreads(FileStream sourceStream, FileStream targetStream, EventHandler<IProgress> notify = null)
        {
            Action<Action<Exception>> action;
            IReader reader;
            IWriter writer;

            countdown = new CountdownEvent(this.ThreadsCount);

            if (notify == null)
            {
                dateProcessor = new DataProcessor();
            }
            else
            {
                long parts = sourceStream.Length / 1048576;
                int notifyFrq = (int)(parts/100 == 0 ? 1 : parts/100);
                dateProcessor = new DataProcessor(notify, notifyFrq, sourceStream.Length);
            }

            if (CompressionMode == CompressionMode.Compress)
            {
                reader = new FileReader(sourceStream, dateProcessor);
                writer = new GZipWriter(targetStream, dateProcessor);

                action = Compressing;
            }
            else
            {
                reader = new GZipReader(sourceStream, dateProcessor);
                writer = new FileWriter(targetStream, dateProcessor);

                action = Decompressing;
            }

            //Запуск потока чтения данных
            Thread readThread = new Thread(()=>reader.Start(ExceptionHandler))
            {
                Name = "Поток чтения файла",
                IsBackground = true
            };
            readThread.Start();

            //Запуск потоков обработки данных
            for (int i = 0; i < this.ThreadsCount; i++)
            {
                Thread thread = new Thread(()=>action(ExceptionHandler))
                {
                    Name = $"Поток процессора {i}",
                    IsBackground = true
                };
                thread.Start();
            }

            //Запуск потока записи данных
            Thread writeThread = new Thread(()=>writer.Start(ExceptionHandler))
            {
                Name = "Поток записи в файл",
                IsBackground = true
            };
            writeThread.Start();

            countdown.Wait();
            writeThread.Join();

            return errorsFlag;
        }

        /// <summary>
        /// Поток сжатия данных
        /// </summary>
        private void Compressing(Action<Exception> handler)
        {
            PartElement partElement;
            do
            {
                if (dateProcessor.IsError) break;

                partElement = dateProcessor.GetProcessingBlock();
                if (partElement == null) continue;
                if (partElement.Idx > -1)
                {
                    try
                    {
                        using (MemoryStream outputMemory = new MemoryStream())
                        {
                            using (GZipStream compressionStream = new GZipStream(outputMemory, CompressionLevel.Optimal))
                            {
                                compressionStream.Write(partElement.Buffer, 0, partElement.Buffer.Length);
                            }
                            partElement.Buffer = outputMemory.ToArray();
                        }
                        dateProcessor.AddProcessedBlock(partElement);
                    }
                    catch (Exception ex)
                    {
                        dateProcessor.SetError();
                        handler(ex);
                        break;
                    }
                }
            } while (partElement.Idx != -1);
            countdown.Signal();
        }

        /// <summary>
        /// Поток разархивирования данных
        /// </summary>
        private void Decompressing(Action<Exception> handler)
        {
            PartElement partElement;
            do
            {
                if (dateProcessor.IsError) break;

                partElement = dateProcessor.GetProcessingBlock();
                if (partElement == null) continue;
                if (partElement.Idx > -1)
                {
                    try
                    {
                        using (GZipStream stream = new GZipStream(new MemoryStream(partElement.Buffer), CompressionMode.Decompress))
                        {
                            using (MemoryStream memory = new MemoryStream())
                            {
                                stream.CopyTo(memory);
                                partElement.Buffer = memory.ToArray();
                            }
                        }
                        dateProcessor.AddProcessedBlock(partElement);
                    }
                    catch (Exception ex)
                    {
                        dateProcessor.SetError();
                        handler(ex);
                        break;
                    }
                }
            } while (partElement.Idx != -1);
            countdown.Signal();
        }

        private void ExceptionHandler(Exception e)
        {
            Interlocked.Exchange(ref errorsFlag, 1);

            Console.WriteLine($"Ошибка - {e.Message}");
            Trace.WriteLine($"Exception in Thread: {e.Message}. Thread ID: {Thread.CurrentThread.ManagedThreadId}({Thread.CurrentThread.Name})\n{e.TargetSite}\n{e.StackTrace}");
            Trace.Flush();
        }
    }

}
